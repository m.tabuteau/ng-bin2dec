import { Component, Input } from "@angular/core";
import { timer } from "rxjs";
import { trigger, transition, style, animate } from "@angular/animations";

@Component({
  selector: "app-result-input",
  template: `
    <div class="control has-icons-right">
      <input
        type="text"
        class="input result"
        [value]="value"
        (click)="onResultClick()"
        readonly
      />
      <span class="icon is-right"><i class="fas fa-copy"></i></span>
      <p @showHideTrigger *ngIf="showCopiedMessage" class="help is-success">
        Copied in the clipboard!
      </p>
    </div>
  `,
  styles: ["input.result { cursor: pointer;}"],
  animations: [
    trigger("showHideTrigger", [
      transition(":enter", [
        style({ opacity: 0 }),
        animate("300ms", style({ opacity: 1 }))
      ]),
      transition(":leave", [animate("300ms", style({ opacity: 0 }))])
    ])
  ]
})
export class ResultInputComponent {
  showCopiedMessage = false;
  @Input() value: string;

  onResultClick() {
    this.showCopiedMessage = true;
    navigator.clipboard.writeText(this.value);

    timer(2000).subscribe(() => {
      this.showCopiedMessage = false;
    });
  }
}
