import { Component, OnInit, Output, EventEmitter } from "@angular/core";

@Component({
  selector: "app-binary-input",
  template: `
    <div class="control">
      <input
        class="input"
        [ngClass]="{ 'is-danger': !isInputValid }"
        type="text"
        placeholder="Binary"
        [(ngModel)]="binaryValue"
        (keyup)="onKeyUp()"
      />
    </div>
    <p *ngIf="!isInputValid" class="help is-danger">Invalid binary number</p>
  `
})
export class BinaryInputComponent {
  binaryValue: string;
  isInputValid: boolean = true;
  @Output() value = new EventEmitter<string>();

  onKeyUp() {
    this.isInputValid = this.checkIsInputValid();

    if (this.isInputValid) {
      this.value.next(this.binaryValue);
    }
  }

  checkIsInputValid(): boolean {
    return /^[0-1]*$/g.test(this.binaryValue);
  }
}
