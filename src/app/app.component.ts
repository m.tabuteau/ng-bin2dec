import { Component } from "@angular/core";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html"
})
export class AppComponent {
  title = "bin2dec";
  result: number = 0;

  onValueChange(value: string) {
    if (value === "") this.result = 0;
    else this.result = this.computeBinaryToDecimal(value);
  }

  computeBinaryToDecimal(binaryValue: string): number {
    return parseInt(binaryValue, 2);
  }
}
