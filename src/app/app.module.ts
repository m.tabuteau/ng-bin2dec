import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";

import { AppComponent } from "./app.component";
import { BinaryInputComponent } from "./binary-input.component";
import { ResultInputComponent } from "./result-input.component";

@NgModule({
  declarations: [AppComponent, BinaryInputComponent, ResultInputComponent],
  imports: [BrowserModule, FormsModule, BrowserAnimationsModule],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
